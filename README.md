# Word Patrol

## Description
Word Patrol is designed to help children enhance their reading skills in an engaging way. As they embark on a fun-filled reading journey, children learn at their own pace, fostering a personalized and interactive learning experience.

## Installation
Word Patrol is comprised of local HTML files, JavaScript, and associated assets. You have multiple options for running the application:
1. Download the files and open them directly in your web browser.
2. Host the files on a Content Delivery Network (CDN).
3. Access the application via its hosted version on CloudFlare Pages at [https://wordpatrol.foss.casa/](https://wordpatrol.foss.casa/).

## Usage
To use Word Patrol, simply add some text or upload a text file. Then, set your desired reading speed in words per minute, and start reading.

## Support
For support, please report any issues on our [GitLab issue tracker](https://gitlab.com/jurjendevries/word-patrol/-/issues). Alternatively, you can contact me via Nostr at [https://nosta.me/npub1l77twp5l02jadkcjn6eeulv2j7y5vmf9tf3hhtq7h7rp0vzhgpzqz0swft](https://nosta.me/npub1l77twp5l02jadkcjn6eeulv2j7y5vmf9tf3hhtq7h7rp0vzhgpzqz0swft).

## Roadmap
Future enhancements include:
- Introducing a range of easy-to-read fonts.
- Implementing Progressive Web App (PWA) functionality for improved accessibility and offline use.

## Contributing
If you are interested in contributing to this project, please fork it and submit your changes via a pull request.

## Authors and Acknowledgments
Word Patrol was developed with the assistance of ChatGPT-4, based on the creative prompts and direction of Jurjen de Vries. The concept is inspired by resources like [Letterprins](https://www.letterprins.nl), [The Codpast](https://thecodpast.org/2016/02/speed-reading-tips-for-dyslexic-readers/), [OpenDyslexic](https://github.com/antijingoist/opendyslexic), [Exceptional Individuals](https://exceptionalindividuals.com/about-us/blog/our-top-10-dyslexia-friendly-fonts/), and [Nielsen Norman Group](https://www.nngroup.com/articles/best-font-for-online-reading/) regarding the use of dyslexia-friendly fonts.

## License
This project is licensed under the MIT License.
